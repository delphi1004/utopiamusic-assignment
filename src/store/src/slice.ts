import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { AppState } from './store'

export interface IState  {
    Countries: {
        [key:string]:string[]
    } | null
}

const initialState: IState = {
  Countries: null
}

const Slice = createSlice({
  name: 'utopia',
  initialState,
  reducers: {
    setCountries: (state, action: PayloadAction<IState['Countries']>) => {
      state.Countries = action.payload
    },
  },
  extraReducers: {},
})

export const selectCountries = (state: AppState) => state.Countries

export const reducer = Slice.reducer
export const actions = Slice.actions
export default Slice
