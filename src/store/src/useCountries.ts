import { useAppDispatch, useAppSelector } from './store'
import { selectCountries, actions, IState } from './slice'

export const useCountries = () => {
  const countries = useAppSelector(selectCountries)
  const dispatch = useAppDispatch()
  const setCountries = (countries:IState["Countries"]) => dispatch(actions.setCountries(countries))

  return {
    countries,
    setCountries,
  }
}