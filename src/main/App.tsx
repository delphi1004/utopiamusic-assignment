import React, { useEffect, useState } from "react"

import styles from './App.module.scss'
import { CountryHighLighter } from '../component'
import { useCountries } from '../store/src/useCountries'
import typography from '../component/styles/typography.module.scss'
import clsx from "clsx"

interface ICountries {
  [key: string]: Array<string>
}

type TDataState = 'unkown' | 'loading' | 'ready' | 'fail'

function App() {
  const [dataState, setDataState] = useState<TDataState>('unkown')
  const { setCountries } = useCountries()

  useEffect(() => {
    const fetchData = async () => {
      setDataState('loading')
      try {
        const response = await fetch('https://api.countries.code-test.utopiamusic.com/all')
        if (response.ok) {
          const result = await response.json()
          const countries: ICountries = {}
          result.forEach((country: { continent: string, name: string }) => {
            if (!countries[country.continent]) {
              countries[country.continent] = []
            }
            countries[country.continent].push(country.name)
          })
          setCountries(countries)
          setDataState('ready')
        } else {
          setDataState('fail')
        }

      } catch (error) {
        setDataState('fail')
      }
    }
    fetchData()
  }, [])

  return (
    <div className={clsx(styles.app)}>
      {dataState === 'loading' &&
        <p className={clsx(styles.app__status, typography.header)}> Loading data... </p>
      }
      {dataState === 'fail' &&
        <p className={clsx(styles.app__status, typography.header)}> Data loading failed </p>
      }
      {dataState === 'ready' &&
        <CountryHighLighter />
      }
    </div>
  )
}

export default App