import React from 'react'

import { Block } from '../../atoms/Block/Block'
import { SectionHeader , CountrySelector } from '../../atoms'

export const CountryHighLighter = () => {
  return (
    <>
      <SectionHeader title={'Utopia Country Highlighter'} />
      <CountrySelector title={'Select region and click on the countries you want to highlight'}/>
    </>
  )
}