import React, { useEffect, useState } from "react"
import clsx from 'clsx'

import styles from './ToggleButton.module.scss'
import typography from '../../styles/typography.module.scss'


export interface IToggleButtonProps {
    title: string;
    initSelected?: boolean;
    onClick?: (name: string | null) => void
}

export const ToggleButton = ({ title, initSelected = false, onClick }: IToggleButtonProps) => {
  const [selected, isSelected] = useState(false)

  useEffect(() => {
    isSelected(initSelected)
  }, [initSelected])

  const clickHandler = () => {
    if (onClick) {
      onClick(!selected ? title : null)
    }
    isSelected(!selected)
  }

  return (
    <div className={
      clsx(styles.container, selected && styles['container--selected']
      )} onClick={clickHandler}
    >
      <p className={clsx(styles['container__title'], typography['button-title'])}>
        {title}
      </p>
    </div >
  )
}