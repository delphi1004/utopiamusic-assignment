import React, { useState } from "react"
import clsx from 'clsx'

import styles from './ToggleLabelButton.module.scss'
import typography from '../../styles/typography.module.scss'


export interface IToggleLabelButtonProps {
  title: string;
}

export const ToggleLabelButton = ({title}:IToggleLabelButtonProps) => {
  const [selected, isSelected] = useState(false)

  return (
    <div className={
      clsx(styles.container, selected && styles['container--selected']
      )} onClick={() => isSelected(!selected)}
    >
      <p className={clsx(styles['container__title'], typography['button-sub-title'])}>
        {title}
      </p>
    </div >
  )
}