import React from 'react'
import clsx from 'clsx'
import { Block } from '../Block/Block'
import typography from '../../styles/typography.module.scss'
import styles from './SectionHeader.module.scss'

interface ISectionHeaderProps {
  title?: string
}

export const SectionHeader = ({ title = "unknown" }: ISectionHeaderProps) => {
  return (
    <Block disableBottomPadding>
      <Block.Content>
        <div className={clsx(styles.container)}>
          <p className={clsx(styles.title, typography['section--header'])}>{title}</p>
          <img className={clsx(styles.logo)} src={require('../../../static/image/logo.png')} alt="" />
        </div>
      </Block.Content>
    </Block>
  )
}