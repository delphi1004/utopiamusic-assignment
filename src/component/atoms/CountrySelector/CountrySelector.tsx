import React, { useState } from 'react'
import clsx from 'clsx'
import { Block } from '../Block/Block'
import typography from '../../styles/typography.module.scss'
import styles from './CountrySelector.module.scss'
import { useCountries } from '../../../store/src/useCountries'

import { ToggleButton } from '../Button/ToggleButton'
import { ToggleLabelButton} from '../Button/ToggleLabelButton'

interface ISectionHeaderProps {
  title?: string
}

export const CountrySelector = ({ title = "unknown" }: ISectionHeaderProps) => {
  const { countries } = useCountries()
  const [selectedRegion, setSelectedRegion] = useState<string | null>(null)
  const regions = countries ? Object.keys(countries) : null

  return (
    <Block disableTopPadding>
      <Block.Content>
        <div className={clsx(styles.container)}>
          <p className={clsx(styles.title, typography['section--sub-header'])}>{title}</p>
        </div>
        <div className={clsx(styles['region-button-container'])}>
          {regions && regions.map(regionName => {
            return <ToggleButton key={regionName} title={regionName}
              initSelected={selectedRegion === regionName}
              onClick={(regionName: string | null) => setSelectedRegion(regionName)} />
          })}
        </div>
        <div className={clsx(styles['country-button-container'])}>
          {selectedRegion && countries && countries[selectedRegion].map(countryName => (
            <ToggleLabelButton key={countryName} title={countryName}/>
          ))}
        </div>
      </Block.Content>
    </Block>
  )
}