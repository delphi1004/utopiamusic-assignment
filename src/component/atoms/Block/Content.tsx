import React from "react"
import clsx from "clsx"
import { ReactNode, useContext } from "react"

import { BlockContext, BlockContextValue } from "./BlockContext"
import styles from "./Content.module.scss"
import Context from "react-redux/es/components/Context"


interface ContentProps {
    children: ReactNode;
}

export function Content(props: ContentProps): JSX.Element | null {
  const { children } = props

  if (!useContext(BlockContext).isInsideBlock) {
    throw new Error("Content should be only used inside the Block.")
  }

  return (
    <div
      className={clsx(styles.container)}
    >
      {children}
    </div>
  )
}
