import { createContext } from "react"

export interface BlockContextValue {
    isInsideBlock: boolean;
}

export const BlockContext = createContext<BlockContextValue>({
  isInsideBlock: false,
})
