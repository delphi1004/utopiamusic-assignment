import React from "react"
import clsx from "clsx"

import { ReactNode, useState } from "react"
import VisibilitySensor from "react-visibility-sensor"
import styles from "./Block.module.scss"
import { Content } from "./Content"
import { BlockContext } from "./BlockContext"

interface BlockProps {
    children: ReactNode;
    disableTopPadding?:boolean;
    disableBottomPadding?:boolean;
    disableAnimation?: boolean;
}
export function Block(props: BlockProps): JSX.Element | null {
  const {
    children,
    disableTopPadding = false,
    disableBottomPadding = false,
    disableAnimation = false,
    ...rest
  } = props
  const [shown, setShown] = useState(false)
  const onVisibilityChange = (isVisible: boolean) => {
    if (isVisible && !shown) {
      setShown(true)
    }
  }

  return (
    <VisibilitySensor partialVisibility onChange={onVisibilityChange}>
      <div
        className={clsx(
          styles.container,
          disableTopPadding && styles['container--disable-top-padding'],
          disableBottomPadding && styles['container--disable-bottom-padding'],
          (shown && !disableAnimation) && styles['container--enable-animation'],
          disableAnimation && styles['container--disable-animation']
          ,
        )}
        {...rest}
      >
        <BlockContext.Provider value={{ isInsideBlock: true }}>
          {children}
        </BlockContext.Provider>
      </div>
    </VisibilitySensor>
  )
}
Block.Content = Content
